import BodyComponent from "./components/BodyComponent";
import '@fontsource/roboto/300.css';
import "bootstrap/dist/css/bootstrap.min.css"

function App() {
  return (
    <div>
      <h1 className="text-center my-5">Pizza Orders List</h1>
      <BodyComponent/>
    </div>
  );
}

export default App;
