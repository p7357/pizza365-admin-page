import AddNew from "./add new/AddNew"
import TableOrder from "./table order/TableOrder"
import axios from "axios"
import {useEffect, useState} from "react"

function BodyComponent() {
    // Mỗi khi post/put/delete sẽ setCrud để gọi lại all order
    const [crud, setCrud] = useState(0)

    const [tableData, setTableData] = useState([])

    const getAllOrder = async() => {
        const response = await axios(`http://42.115.221.44:8080/devcamp-pizza365/orders`);
        const responseData = response.data;
        return responseData;
    }

    useEffect(() => {
        getAllOrder()
        .then((data) => {
            setTableData(data)
        })
    },[crud])

    return (
        <div>
            <AddNew setCrud={setCrud} crud={crud}></AddNew>
            <TableOrder tableData={tableData} setCrud={setCrud} crud={crud}></TableOrder>
        </div>
    )
}

export default BodyComponent