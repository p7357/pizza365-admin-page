import { Button} from "@mui/material"
import AddIcon from '@mui/icons-material/Add';
import { useState } from "react"
import InsertModal from "./modal/InsertModal"

const btnStyle = {
    marginBottom: "20px",
    marginLeft: "20px"
}

function AddNew({ setCrud, crud }) {
    const [openModal, setOpenModal] = useState(false);

    return (
        <div>
            <Button onClick={() => setOpenModal(true)} style={btnStyle} variant="contained" color="success"><AddIcon />New Order</Button>
            <InsertModal openModal={openModal} setOpenModal={setOpenModal} setCrud={setCrud} crud={crud}></InsertModal>
        </div>
    )
}

export default AddNew