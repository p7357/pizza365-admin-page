import { Alert, Button, Grid, MenuItem, Modal, Stack, TextField, Typography, Snackbar } from "@mui/material"
import Box from '@mui/material/Box';
import axios from "axios"
import { useEffect, useState } from "react";
import sizeList from "../../../sizeData.json"
import typeList from "../../../typeData.json"

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    height: 550,
    bgcolor: 'white',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function InsertModal({ openModal, setOpenModal, setCrud, crud}) {
    const [openError, setOpenError] = useState(false);

    const [openSuccess, setOpenSuccess] = useState(false);

    const [openDelay, setOpenDelay] = useState(false);

    const closeAlert = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSuccess(false);
        setOpenError(false);
        setOpenDelay(false);
    };

    const [drinkList, setDrinkList] = useState([])

    const [customer, setCustomer] = useState({
        name: "",
        phone: "",
        address: "",
        email: "",
        voucher: "",
        note: ""
    })

    const [combo, setCombo] = useState({
        size: "",
        width: "",
        rib: "",
        salad: "",
        glasses: "",
        price: ""
    })

    const [type, setType] = useState("")

    const [drink, setDrink] = useState("")

    const [error, setError] = useState({
        name: "First name is empty!",
        phone: "Phone number is empty!",
        address: "Address is empty!",
        email: "none",
        voucher: "none",
        size: "You haven't selected combo size yet!",
        type: "You haven't selected pizza type yet!",
        drink: "You haven't selected drink type yet!"
    })

    const [newOrder, setNewOrder] = useState({})

    const closeModal = () => {
        setOpenModal(false);
        setError({
            name: "Full name is empty!",
            phone: "Phone number is empty!",
            address: "Address is empty!",
            email: "none",
            voucher: "none",
            size: "You haven't selected combo size yet!",
            type: "You haven't selected pizza type yet!",
            drink: "You haven't selected drink type yet!"
        })
        setCustomer({
            name: "",
            phone: "",
            address: "",
            email: "",
            voucher: "",
            note: ""
        })
        setCombo({
            size: "",
            width: "",
            rib: "",
            salad: "",
            glasses: "",
            price: ""
        })
        setType("");
        setDrink("");
    }

    const getDrink = async () => {
        const response = await axios(`http://42.115.221.44:8080/devcamp-pizza365/drinks`);
        const data = response.data;
        return data
    }

    const changeSize = (event) => {
        for (let bI = 0; bI < sizeList.length; bI++) {
            if (event.target.value === sizeList[bI].size) {
                setCombo(sizeList[bI])
            }
        }
        if (event.target.value !== "") {
            setError({ ...error, size: "none" })
        }
    }

    const changeType = (event) => {
        setType(event.target.value)
        if (event.target.value !== "") {
            setError({ ...error, type: "none" })
        }
    }

    const changeDrink = (event) => {
        setDrink(event.target.value)
        if (event.target.value !== "") {
            setError({ ...error, drink: "none" })
        }
    }

    const changeName = (event) => {
        setCustomer({ ...customer, name: event.target.value })
        if (event.target.value === "") {
            setError({ ...error, name: "Full name is empty!" })
        } else {
            setError({ ...error, name: "none" })
        }
    }

    const changePhone = (event) => {
        setCustomer({ ...customer, phone: event.target.value })
        if (event.target.value === "") {
            setError({ ...error, phone: "Phone number is empty!" })
        } else if (isNaN(event.target.value) || event.target.value.length !== 10) {
            setError({ ...error, phone: "The phone number must be a ten-digit number!" })
        } else {
            setError({ ...error, phone: "none" })
        }
    }

    const changeAddress = (event) => {
        setCustomer({ ...customer, address: event.target.value })
        if (event.target.value === "") {
            setError({ ...error, address: "Address is empty!" })
        } else {
            setError({ ...error, address: "none" })
        }
    }

    const changeEmail = (event) => {
        setCustomer({ ...customer, email: event.target.value })
        if (/\S+@\S+\.\S+/.test(event.target.value) || event.target.value === "") {
            setError({ ...error, email: "none" })
        } else {
            setError({ ...error, email: "Email syntax isn't correct" })
        }
    }

    const changeVoucher = (event) => {
        setCustomer({ ...customer, voucher: event.target.value })
        if (isNaN(event.target.value) === false || event.target.value === "") {
            setError({ ...error, voucher: "none" })
        } else {
            setError({ ...error, voucher: "Voucher must be a number" })
        }
    }

    const changeNote = (event) => {
        setCustomer({ ...customer, note: event.target.value })
    }

    const createNewOrder = async() => {
        const response = await axios(`http://42.115.221.44:8080/devcamp-pizza365/orders`, {
            method: `POST`,
            data: {
                kichCo: combo.size,
                duongKinh: combo.width,
                suon: combo.rib,
                salad: combo.salad,
                loaiPizza: type,
                idVourcher: customer.voucher,
                idLoaiNuocUong: drink,
                soLuongNuoc: combo.glasses,
                hoTen: customer.name,
                thanhTien: combo.price,
                email: customer.email,
                soDienThoai: customer.phone,
                diaChi: customer.address,
                loiNhan: customer.note     
            },
            header: {"Content-Type": "application/json;charset=UTF-8"} 
        })
        const data = response.data;
        return data
    }

    const insertClick = () => {
        if (error.name === "none" && error.phone === "none" && error.address === "none" && error.email === "none" &&
            error.voucher === "none" && error.size === "none" && error.type === "none" && error.drink === "none") {
            createNewOrder()
            .then((data) => {
                setNewOrder(data)
                setOpenSuccess(true)
                setCrud(crud + 1)
                closeModal()
            })
            .catch((error) => {
                setOpenDelay(true);
                closeModal()
            })
        } else {
            setOpenError(true)
        }
    }


    useEffect(() => {
        getDrink()
            .then((data) => {
                setDrinkList(data)
            })
    }, [])

    return (
        <div>
            {/* Add new user modal */}
            <Modal open={openModal} onClose={closeModal}>
                <Box sx={style}>
                    <Typography variant="h6" className="text-center border-bottom" style={{ marginBottom: "20px" }}>
                        <b>Insert New Order</b>
                    </Typography>
                    <Grid container>
                        <Grid container className="border-bottom py-3">
                            <Grid item xs={3} style={{ marginLeft: "70px", marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField onChange={changeName} size="small" label="Full name" variant="outlined"></TextField>
                                    <TextField onChange={changePhone} size="small" label="Phone number" variant="outlined"></TextField>
                                </Stack>
                            </Grid>
                            <Grid item xs={3} style={{ marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField onChange={changeAddress} size="small" label="Address" variant="outlined"></TextField>
                                    <TextField onChange={changeEmail} size="small" label="Email" variant="outlined"></TextField>
                                </Stack>
                            </Grid>
                            <Grid item xs={3} >
                                <Stack spacing={3}>
                                    <TextField onChange={changeVoucher} size="small" label="Voucher" variant="outlined"></TextField>
                                    <TextField onChange={changeNote} size="small" label="Note" variant="outlined"></TextField>
                                </Stack>
                            </Grid>
                        </Grid>
                        <Grid container className="border-bottom py-3">
                            <Grid item xs={3} style={{ marginLeft: "70px", marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField onChange={changeSize} value={combo.size} size="small" label="Combo Size" variant="outlined" select>
                                        {sizeList.map((element, index) => {
                                            return <MenuItem key={index} value={element.size}>{element.size}</MenuItem>
                                        })}
                                    </TextField>
                                    <TextField size="small" label="Width (Cm)" value={combo.width} variant="outlined" disabled></TextField>
                                </Stack>
                            </Grid>
                            <Grid item xs={3} style={{ marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField value={combo.rib} size="small" label="Rib (Pieces)" variant="outlined" disabled></TextField>
                                    <TextField value={combo.salad} size="small" label="Salad (Gram)" variant="outlined" disabled></TextField>
                                </Stack>
                            </Grid>
                            <Grid item xs={3} >
                                <Stack spacing={3}>
                                    <TextField value={combo.glasses} size="small" label="Drink (Glasses)" variant="outlined" disabled></TextField>
                                    <TextField value={combo.price} size="small" label="Price (VND)" variant="outlined" disabled></TextField>
                                </Stack>
                            </Grid>
                        </Grid>
                        <Grid container className="border-bottom py-3">
                            <Grid item xs={3} style={{ marginLeft: "70px", marginRight: "50px" }}>
                                <TextField onChange={changeType} value={type} fullWidth size="small" label="Pizza Type" variant="outlined" select>
                                    {typeList.map((element, index) => {
                                        return <MenuItem key={index} value={element.typeId}>{element.typeName}</MenuItem>
                                    })}
                                </TextField>
                            </Grid>
                            <Grid item xs={3} style={{ marginRight: "50px" }}>
                                <TextField onChange={changeDrink} value={drink} fullWidth size="small" label="Drink Type" variant="outlined" select>
                                    {drinkList.map((element, index) => {
                                        return <MenuItem key={index} value={element.maNuocUong}>{element.tenNuocUong}</MenuItem>
                                    })}
                                </TextField>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justifyContent="flex-end" className="my-4">
                        <Button onClick={insertClick} variant="contained" color="success" className="mx-3"> Create </Button>
                        <Button onClick={closeModal} variant="contained" style={{ backgroundColor: "grey" }}> Cancel </Button>
                    </Grid>
                </Box>
            </Modal>

            {/* Alert Success, Error, Warning */}
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={closeAlert}>
                <Alert variant="filled" severity="success" sx={{ width: '100%' }}>
                    <Typography>Create New Order Successful</Typography>
                    <Typography>ID: {newOrder.id}</Typography>
                </Alert>
            </Snackbar>
            <Snackbar open={openError} autoHideDuration={6000} onClose={closeAlert}>
                <Alert variant="filled" severity="warning" sx={{ width: '100%' }}>
                    <Typography>{error.name !== "none" ? error.name : null}</Typography>
                    <Typography>{error.phone !== "none" ? error.phone : null}</Typography>
                    <Typography>{error.address !== "none" ? error.address : null}</Typography>
                    <Typography>{error.email !== "none" ? error.email : null}</Typography>
                    <Typography>{error.voucher !== "none" ? error.voucher : null}</Typography>
                    <Typography>{error.size !== "none" ? error.size : null}</Typography>
                    <Typography>{error.type !== "none" ? error.type : null}</Typography>
                    <Typography>{error.drink !== "none" ? error.drink : null}</Typography>
                </Alert>
            </Snackbar>
            <Snackbar open={openDelay} autoHideDuration={6000} onClose={closeAlert}>
                <Alert variant="filled" severity="error" sx={{ width: '100%' }}>
                    <Typography>Can't create new order right away. Please try again later</Typography>
                </Alert>
            </Snackbar>
        </div>
    )
}

export default InsertModal