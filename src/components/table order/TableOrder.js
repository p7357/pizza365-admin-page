import { Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material"
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import {useState} from "react"
import UpdateModal from "./modal/UpdateModal";
import DeleteModal from "./modal/DeleteModal";

function TableOrder({ tableData, setCrud, crud }) {
    const [openUpdateModal, setOpenUpdateModal] = useState(false);

    const [openDeleteModal, setOpenDeleteModal] = useState(false);

    const [rowData, setRowData] = useState({})

    const editClick = (element) => {
        setRowData(element)
        setOpenUpdateModal(true)
    }

    const deleteClick = (element) => {
        setRowData(element)
        setOpenDeleteModal(true)
    }

    return (
        <div>
            <Table>
                <TableHead >
                    <TableRow >
                        <TableCell><b>ID</b></TableCell>
                        <TableCell><b>Order ID</b></TableCell>
                        <TableCell><b>Size</b></TableCell>
                        <TableCell><b>Type</b></TableCell>
                        <TableCell><b>Drink</b></TableCell>
                        <TableCell><b>Name</b></TableCell>
                        <TableCell><b>Phone</b></TableCell>
                        <TableCell><b>Total</b></TableCell>
                        <TableCell><b>Status</b></TableCell>
                        <TableCell><b>Action</b></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {tableData.map((element, index) => {
                        return <TableRow key={index}>
                            <TableCell>{element.id}</TableCell>
                            <TableCell>{element.orderId}</TableCell>
                            <TableCell>{element.kichCo}</TableCell>
                            <TableCell>{element.loaiPizza}</TableCell>
                            <TableCell>{element.idLoaiNuocUong}</TableCell>
                            <TableCell>{element.hoTen}</TableCell>
                            <TableCell>{element.soDienThoai}</TableCell>
                            <TableCell>{element.giamGia ? (element.thanhTien - element.giamGia) : element.thanhTien}</TableCell>
                            <TableCell>{element.trangThai}</TableCell>
                            <TableCell> 
                                <EditIcon onClick={() => editClick(element)} style={{color:"blue"}} /> 
                                <DeleteIcon onClick={() => deleteClick(element)} style={{color:"red"}}/> 
                            </TableCell>
                        </TableRow>
                    })}
                </TableBody>
            </Table>
            <UpdateModal rowData={rowData} setRowData={setRowData} openUpdateModal={openUpdateModal} setOpenUpdateModal={setOpenUpdateModal} crud={crud} setCrud={setCrud}></UpdateModal>
            <DeleteModal rowData={rowData} openDeleteModal={openDeleteModal} setOpenDeleteModal={setOpenDeleteModal} crud={crud} setCrud={setCrud}></DeleteModal>
        </div>
    )
}

export default TableOrder