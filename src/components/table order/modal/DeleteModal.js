import { Alert, Button, Grid, Modal, Typography, Snackbar } from "@mui/material"
import Box from '@mui/material/Box';
import axios from "axios"
import { useState } from "react";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    height: 150,
    bgcolor: 'white',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function DeleteModal({ openDeleteModal, setOpenDeleteModal, setCrud, crud, rowData }) {
    const [openError, setOpenError] = useState(false);

    const [openSuccess, setOpenSuccess] = useState(false);

    const closeAlert = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSuccess(false);
        setOpenError(false);
    };

    const closeDeleteModal = () => {
        setOpenDeleteModal(false)
    }

    const deleteOrder = async() => {
        const response = await axios(`http://42.115.221.44:8080/devcamp-pizza365/orders/${rowData.id}`,{
            method: `DELETE`
        });
        const data = response.data;
        return data;
    }

    const deleteClick = () => {
        deleteOrder()
        .then((data) => {
            setOpenSuccess(true);
            setCrud(crud + 1);
            closeDeleteModal();
        })
        .catch((error) => {
            setOpenError(true);
            closeDeleteModal();
        })
    }

    return (
        <div>
            {/* Delete Modal */}
            <Modal open={openDeleteModal} onClose={closeDeleteModal}>
                <Box sx={style}>
                    <Typography variant="h6" className="text-center border-bottom" style={{ marginBottom: "20px" }}>
                        <b>Are you sure you want to delete this order?</b>
                    </Typography>
                    <Grid container justifyContent="flex-end" className="my-4">
                        <Button onClick={deleteClick} variant="contained" color="error" className="mx-3"> Delete </Button>
                        <Button onClick={closeDeleteModal} variant="contained" style={{ backgroundColor: "grey" }}> Cancel </Button>
                    </Grid>
                </Box>
            </Modal>

            {/* Alert Success & Error */}
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={closeAlert}>
                <Alert variant="filled" severity="success" sx={{ width: '100%' }}>
                    <Typography>Delete Order Successful</Typography>
                </Alert>
            </Snackbar>
            <Snackbar open={openError} autoHideDuration={6000} onClose={closeAlert}>
                <Alert variant="filled" severity="warning" sx={{ width: '100%' }}>
                    <Typography>Can't delete order right away. Please try again later</Typography>
                </Alert>
            </Snackbar>
        </div>
    )
}

export default DeleteModal