import { Alert, Button, Grid, MenuItem, Modal, Stack, TextField, Typography, Snackbar } from "@mui/material"
import Box from '@mui/material/Box';
import axios from "axios"
import { useState } from "react";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1300,
    height: 600,
    bgcolor: 'white',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function UpdateModal({ openUpdateModal, setOpenUpdateModal, setCrud, crud, rowData, setRowData }) {
    const statusList = [
        {statusName: "Open", statusId: "open"},
        {statusName: "Đã hủy", statusId: "cancel"},
        {statusName: "Đã xác nhận", statusId: "confirmed"}
    ]

    const [openError, setOpenError] = useState(false);

    const [openSuccess, setOpenSuccess] = useState(false);

    const closeAlert = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSuccess(false);
        setOpenError(false);
    };


    const closeUpdateModal = () => {
        setOpenUpdateModal(false)
    }

    const changeStatus = (event) => {
        setRowData({...rowData, trangThai: event.target.value})
    }

    const updateOrder = async() => {
        const response = await axios (`http://42.115.221.44:8080/devcamp-pizza365/orders/${rowData.id}`, {
            method: `PUT`,
            data: {
                trangThai: rowData.trangThai
            },
            header: {
                "Content-Type": "application/json;charset=UTF-8"
            }
        })
        const data = response.data;
        return data;
    }

    const updateClick = () => {
        updateOrder()
        .then((data) => {
            setOpenSuccess(true);
            setCrud(crud + 1);
            closeUpdateModal()
        })
        .catch((error) => {
            setOpenError(true);
            closeUpdateModal()
        })
    }

    return (
        <div>
            {/* Update Modal */}
            <Modal open={openUpdateModal} onClose={closeUpdateModal}>
                <Box sx={style}>
                    <Typography variant="h6" className="text-center border-bottom" style={{ marginBottom: "20px" }}>
                        <b>Update Order</b>
                    </Typography>
                    <Grid container>
                        <Grid container className="border-bottom py-3">
                            <Grid item xs={2} style={{ marginLeft: "70px", marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField disabled value={rowData.id} size="small" label="ID" variant="outlined"></TextField>
                                    <TextField disabled value={rowData.orderId} size="small" label="Order ID" variant="outlined"></TextField>
                                </Stack>
                            </Grid>

                            <Grid item xs={2} style={{ marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField disabled value={rowData.hoTen} size="small" label="Full name" variant="outlined"></TextField>
                                    <TextField disabled value={rowData.soDienThoai} size="small" label="Phone number" variant="outlined"></TextField>
                                    <TextField disabled value={rowData.diaChi} size="small" label="Address" variant="outlined"></TextField>
                                </Stack>
                            </Grid>

                            <Grid item xs={2} style={{ marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField disabled value={rowData.email} size="small" label="Email" variant="outlined"></TextField>
                                    <TextField disabled value={rowData.idVourcher} size="small" label="Voucher" variant="outlined"></TextField>
                                    <TextField disabled value={rowData.loiNhan} size="small" label="Note" variant="outlined"></TextField>
                                </Stack>
                            </Grid>

                            <Grid item xs={2}>
                                <Stack spacing={3}>
                                    <TextField disabled value={rowData.ngayTao} size="small" label="Ngày tạo" variant="outlined"></TextField>
                                    <TextField disabled value={rowData.ngayCapNhat} size="small" label="Ngày cập nhật" variant="outlined"></TextField>
                                    <TextField onChange={changeStatus} value={rowData.trangThai} size="small" label="Status" variant="outlined" select>
                                        {statusList.map((element, index) => {
                                            return <MenuItem key={index} value={element.statusId}>{element.statusName}</MenuItem>
                                        })}
                                    </TextField>
                                </Stack>
                            </Grid>

                        </Grid>
                        <Grid container className="border-bottom py-3">
                            <Grid item xs={2} style={{ marginLeft: "70px", marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField disabled value={rowData.kichCo} size="small" label="Combo Size" variant="outlined"></TextField>
                                    <TextField value={rowData.duongKinh} size="small" label="Width (Cm)" variant="outlined" disabled></TextField>
                                </Stack>
                            </Grid>
                            <Grid item xs={2} style={{ marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField value={rowData.suon} size="small" label="Rib (Pieces)" variant="outlined" disabled></TextField>
                                    <TextField value={rowData.salad} size="small" label="Salad (Gram)" variant="outlined" disabled></TextField>
                                </Stack>
                            </Grid>
                            <Grid item xs={2} style={{ marginRight: "50px" }}>
                                <Stack spacing={3}>
                                    <TextField value={rowData.soLuongNuoc} size="small" label="Drink (Glasses)" variant="outlined" disabled></TextField>
                                    <TextField value={rowData.thanhTien} size="small" label="Price (VND)" variant="outlined" disabled></TextField>
                                </Stack>
                            </Grid>
                            <Grid item xs={2} >
                                <Stack spacing={3}>
                                    <TextField value={rowData.idLoaiNuocUong} size="small" label="Drink Type" variant="outlined" disabled></TextField>
                                    <TextField value={rowData.loaiPizza} size="small" label="Pizza Type" variant="outlined" disabled></TextField>
                                    <TextField value={rowData.giamGia} size="small" label="Discount (VND)" variant="outlined" disabled></TextField>
                                </Stack>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justifyContent="flex-end" className="my-4">
                        <Button onClick={updateClick} variant="contained" color="success" className="mx-3"> Update </Button>
                        <Button onClick={closeUpdateModal} variant="contained" style={{ backgroundColor: "grey" }}> Cancel </Button>
                    </Grid>
                </Box>
            </Modal>

            {/* Alert Success & Error*/}
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={closeAlert}>
                <Alert variant="filled" severity="success" sx={{ width: '100%' }}>
                    <Typography>Update Order Successful</Typography>
                </Alert>
            </Snackbar>
            <Snackbar open={openError} autoHideDuration={6000} onClose={closeAlert}>
                <Alert variant="filled" severity="warning" sx={{ width: '100%' }}>
                    <Typography>Can't update order right away. Please try again later</Typography>
                </Alert>
            </Snackbar>
        </div >
    )
}

export default UpdateModal